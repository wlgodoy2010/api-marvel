package br.com.marvel.avengers.domain.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="tb_events")
@Data
public class Events {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idEvent")
    private Long idEvent;
    @Column(name="titulo")
    private String titulo;
    @Column(name="edicao")
    private Long edicao;
    @ManyToOne
    @JoinColumn(name = "idCharacter")
    private  Character idCh;

}
