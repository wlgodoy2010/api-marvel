package br.com.marvel.avengers.domain.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="tb_comics")
@Data
public class Comics {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idCommic;
    private String titulo;
    private Long edicao;
    @ManyToOne
    @JoinColumn(name = "idCharacter")
    private  Character idCh;




}
