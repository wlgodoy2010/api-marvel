package br.com.marvel.avengers.domain.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="tb_series")
@Data
public class Series {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idSeries;
    private String titulo;

    @ManyToOne
    @JoinColumn(name = "idCharacter")
    private  Character idCh;

}
