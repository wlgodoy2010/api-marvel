package br.com.marvel.avengers.domain.models;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tb_character")
@Data
public class Character implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_character")
    private Long idCharacter;
    @Column(name = "nome")
    private String nome;
    @Column(name = "profissao")
    private String profissao;
    @Column(name = "alterEgo")
    private String alterEgo;
    @OneToMany(mappedBy = "idCh")
    private List<Comics> comics;
    @OneToMany(mappedBy = "idCh")
    private List<Events> events;
    @OneToMany(mappedBy = "idCh")
    private List<Series> series;
    @OneToMany(mappedBy = "idCh")
    private List<Stories> stories;
}
