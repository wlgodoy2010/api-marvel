package br.com.marvel.avengers.domain.models;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name= "tb_stories")
@Data
public class Stories {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idStories")
    private Long idStories;
    @Column(name="titulo")
    private String titulo;
    @Column(name="edicao")
    private Long edicao;
    @ManyToOne
    @JoinColumn(name = "idCharacter")
    private  Character idCh;
}
