package br.com.marvel.avengers.service;

import br.com.marvel.avengers.domain.models.Character;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CharactersService {

    Optional<Character> buscarPorId(Long idCharacter);

    List<Character> findAll();
}