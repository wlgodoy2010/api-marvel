package br.com.marvel.avengers.service;

import br.com.marvel.avengers.domain.models.Character;
import br.com.marvel.avengers.domain.models.Comics;

import java.util.Optional;

public interface ComicsService {

    Optional<Comics> buscarPorId(Long idCharacter);
}
