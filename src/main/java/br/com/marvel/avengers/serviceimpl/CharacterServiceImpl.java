package br.com.marvel.avengers.serviceimpl;

import br.com.marvel.avengers.domain.models.Character;
import br.com.marvel.avengers.repository.CharacterRepository;
import br.com.marvel.avengers.service.CharactersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CharacterServiceImpl  implements CharactersService {

    @Autowired
    private CharacterRepository characterRepository;

    @Override
    public Optional<Character> buscarPorId(Long idCharacter) {
        return this.characterRepository.findById(idCharacter);
    }

    @Override
    public List<Character> findAll() {
        return characterRepository.findAll();
    }
}
