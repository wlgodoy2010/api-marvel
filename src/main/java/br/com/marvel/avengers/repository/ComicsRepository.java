package br.com.marvel.avengers.repository;

import br.com.marvel.avengers.domain.models.Comics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ComicsRepository  extends JpaRepository<Comics, Long> {
    Optional<Comics> buscarPorId(Long idCharacter);
}
