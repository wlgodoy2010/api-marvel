package br.com.marvel.avengers.resource;

import br.com.marvel.avengers.domain.models.Character;
import br.com.marvel.avengers.serviceimpl.CharacterServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping(value = "v1/public/characters")
public class CharacterResource {

    @Autowired
    private CharacterServiceImpl service;



    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/", produces = "application/json")
    public ResponseEntity<List<Character>> listar() {
        List<Character> list = service.findAll();

        return ResponseEntity.ok().body(list);
    }

   @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{characterId}", produces = "application/json")
    public ResponseEntity<Object> findById(@PathVariable Long characterId) {
        Optional<Character> id = service.buscarPorId(characterId);
        return ResponseEntity.ok().body(id);
    }










}

